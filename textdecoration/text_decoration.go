package textdecoration

import (
	"bytes"
)

// This function will process each character and, where relevant, will transform org markup
// to HTML.
//
// For example:
//  *this is bold text* becomes <b>this is bold text</b>
//  /this is italicized text/ becomes <i>this is italicized text</i>
//  ~this is inline code~ becomes <code>this is inline code</code>
//
// It is expected that the boundaries of the text has already been defined before being passed
// into this function. As a result, no error checking will be done here.
func Decorate(s []byte, buf *bytes.Buffer) {
	var boldHits []int
	var italicHits []int
	var codeHits []int

	for i := 0; i < len(s); i++ {
		switch s[i] {
		// If char is an asterisk
		case 42:
			if !queryByte(s, i, &boldHits, buf) {
				buf.WriteByte(s[i])
			}

		// If char is a forward slash
		case 47:
			if !queryByte(s, i, &italicHits, buf) {
				buf.WriteByte(s[i])
			}

		// If char is a tilde
		case 126:
			if !queryByte(s, i, &codeHits, buf) {
				buf.WriteByte(s[i])
			}

		default:
			buf.WriteByte(s[i])
		}
	}
}

// This function will examine the byte at s[pos] and check to see if it's the beginning of a text
// decoration. If it is, we look forward for a possible closing byte.
//
// If s[pos] has a valid end byte, we transform s[pos] into a HTML tag and write it to the buffer.
// We also check to see if s[pos] is an end byte and, if so, transform that to a HTML tag and
// write it to the buffer, too.
//
// We return a boolean based on whether s[pos] does designate a text decoration.
func queryByte(s []byte, pos int, collection *[]int, buf *bytes.Buffer) bool {
	var tag []byte

	// Here we are determining the opening and closing tag for the decoration.
	switch s[pos] {
	// If char is an asterisk
	case 42:
		// This write is the same as "b"
		tag = []byte{98}

	// If char is a forward slash
	case 47:
		// This write is the same as "i"
		tag = []byte{105}

	// If char is a tilde
	case 126:
		// This write is the same as "code"
		tag = []byte{99, 111, 100, 101}
	}

	// If s[pos] is not in the collection, it means that we haven't seen it yet.
	// The only reason we'd see s[pos] is if we looked forward for an end byte.
	if !inArray(pos, *collection) {
		if pos != len(s)-1 {
			if isValidStartChar(s, pos, s[pos]) {

				// Here we are looking forward for the end byte.
				// We stop when we find the first valid byte.
				for j := pos + 1; j < len(s); j++ {
					if isValidEndChar(s, j, s[pos]) {
						// This block of writes is the same as <tag>
						buf.WriteByte(60)
						buf.Write(tag)
						buf.WriteByte(62)

						*collection = append(*collection, j)
						return true
					}
				}
			}
		}
	} else {
		// This block of writes is the same as </tag>
		buf.Write([]byte{60, 47})
		buf.Write(tag)
		buf.WriteByte(62)

		return true
	}

	return false
}

// Checks whether the given byte (s[pos]) is considered a valid start tag
func isValidStartChar(s []byte, pos int, b byte) bool {
	// Checks if s[pos] == b (*, /, or ~) and s[pos+1] is not a whitespace
	if s[pos] == b && s[pos+1] != 32 {

		// Checks if the char before s[pos] is within bounds, and
		// is not alphanumeric. If it isn't within bounds, it's
		// considered valid.
		if pos-1 > 0 {
			return !isAlphanumeric(s[pos-1])
		} else {
			return true
		}
	}

	return false
}

// Checks whether the given byte (s[pos]) is considered a valid end tag
func isValidEndChar(s []byte, pos int, b byte) bool {
	// Checks if s[pos] == b (*, /, or ~) and s[pos-1] is not a whitespace
	if s[pos] == b && s[pos-1] != 32 {

		// Checks if the char after s[pos] is within bounds, and
		// is not alphanumeric. If it isn't within bounds, it's
		// considered valid.
		if pos+1 < len(s) {
			return !isAlphanumeric(s[pos+1])
		} else {
			return true
		}
	}

	return false
}

// Checks whether the given byte is alpha numeric (/0-9/, /a-z/ or /A-Z/)
func isAlphanumeric(b byte) bool {
	if (b > 65 && b < 91) || (b > 96 && b < 123) || (b > 47 && b < 58) {
		return true
	}

	return false
}

// Checks to see whether the given position is in the given array. Returns true if it is, false if not.
func inArray(p int, a []int) bool {
	for _, c := range a {
		if p == c {
			return true
		}
	}

	return false
}
