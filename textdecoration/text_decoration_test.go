package textdecoration

import (
	"bytes"
	"testing"
)

func TestDecorate(t *testing.T) {
	var buf bytes.Buffer

	tt := []struct {
		s    []byte
		expS string
	}{
		{[]byte("*this will be bold*"), "<b>this will be bold</b>"},
		{[]byte("*this will not be bold"), "*this will not be bold"},
		{[]byte("~this is code~"), "<code>this is code</code>"},
		{[]byte("so ~is this~"), "so <code>is this</code>"},
		{[]byte("/this is italic/"), "<i>this is italic</i>"},
	}

	for _, test := range tt {
		Decorate(test.s, &buf)

		if buf.String() != test.expS {
			t.Fatalf("resp: %s\nexpResp: %s", buf.String(), test.expS)
		}

		buf.Reset()
	}
}

func TestQueryByte(t *testing.T) {
	var buf bytes.Buffer

	tt := []struct {
		s       []byte
		expS    string
		col     []int
		pos     int
		expResp bool
	}{
		{[]byte("*this is valid*"), "<b>", []int{}, 0, true},
		{[]byte("/this is valid/"), "<i>", []int{}, 0, true},
		{[]byte("~this is valid~"), "<code>", []int{}, 0, true},
		{[]byte("*this is not valid"), "", []int{}, 0, false},
		{[]byte("*"), "</b>", []int{0}, 0, true},
		{[]byte("/"), "</i>", []int{0}, 0, true},
		{[]byte("~"), "</code>", []int{0}, 0, true},
		{[]byte("this is an end tag~"), "</code>", []int{18}, 18, true},
	}

	for _, test := range tt {
		resp := queryByte(test.s, test.pos, &test.col, &buf)

		if resp != test.expResp {
			t.Fatalf("resp is %t not %t", resp, test.expResp)
		}

		if buf.String() != test.expS {
			t.Fatalf("resp: %s\nexpResp: %s", buf.String(), test.expS)
		}

		buf.Reset()
	}
}

func TestIsValidStartChar(t *testing.T) {
	tt := []struct {
		s       []byte
		b       byte
		pos     int
		expResp bool
	}{
		{[]byte("*this*"), []byte("*")[0], 0, true},
		{[]byte(" *this*"), []byte("*")[0], 1, true},
		{[]byte("not*this"), []byte("*")[0], 3, false},
		{[]byte(">*this"), []byte("*")[0], 1, true},
	}

	for _, test := range tt {
		resp := isValidStartChar(test.s, test.pos, test.b)

		if resp != test.expResp {
			t.Fatalf("%+v is %t", test, resp)
		}
	}
}

func TestIsValidEndChar(t *testing.T) {
	tt := []struct {
		s       []byte
		b       byte
		pos     int
		expResp bool
	}{
		{[]byte("*this*"), []byte("*")[0], 5, true},
		{[]byte("*this* "), []byte("*")[0], 5, true},
		{[]byte("*not*this"), []byte("*")[0], 4, false},
		{[]byte("this*>"), []byte("*")[0], 4, true},
	}

	for _, test := range tt {
		resp := isValidEndChar(test.s, test.pos, test.b)

		if resp != test.expResp {
			t.Fatalf("%+v is %t", test, resp)
		}
	}
}

func TestIsAlphanumeric(t *testing.T) {
	tt := []struct {
		char    string
		expResp bool
	}{
		{"a", true},
		{"<", false},
		{"J", true},
		{"Z", true},
		{"!", false},
		{"n", true},
		{" ", false},
		{"9", true},
		{"0", true},
		{"C", true},
		{"z", true},
	}

	for _, test := range tt {
		resp := isAlphanumeric([]byte(test.char)[0])

		if resp != test.expResp {
			t.Fatalf("%+v is %t", test, resp)
		}
	}
}

func TestInArray(t *testing.T) {
	tt := []struct {
		val     int
		arr     []int
		expResp bool
	}{
		{1, []int{0, 1, 2, 3, 4, 5}, true},
		{5, []int{0, 0, 0, 0, 0, 0}, false},
		{1000, []int{10, 100, 1000, 10000}, true},
		{0, []int{1, 2, 3, 4, 5, 6}, false},
	}

	for _, test := range tt {
		resp := inArray(test.val, test.arr)

		if resp != test.expResp {
			t.Fatalf("%+v is %t", test, resp)
		}
	}
}
