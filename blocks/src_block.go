package blocks

import (
	"bytes"
)

// This function creates a HTML formatted code block.
//
// It transforms this:
//  #+BEGIN_SRC emacs-lisp
//  (+ 1 2 3 4 5)
//  #+END_SRC
//
// Into this:
//  <code class="emacs-lisp"><pre>
//  (+ 1 2 3 4 5)
//  </pre></code>
//
// It is expected that you pass the position of the character after the whitespace to save steps.
// Example, everything within the square brackets gets passed over:
//  [#+BEGIN_SRC ]emacs-lisp
//
// It will return the position after the end of the block (most likely a newline).
// (#+END_SRC)
func createSrcBlock(s []byte, pos int, buf *bytes.Buffer) int {
	language, pos := getLanguage(s, pos)

	// This block of writes is the same as "<code class="language">"
	buf.Write([]byte{60, 99, 111, 100, 101, 32, 99, 108, 97, 115, 115, 61, 34})
	buf.Write(language)
	buf.Write([]byte{34, 62})

	// This write is the same as "<pre>"
	buf.Write([]byte{60, 112, 114, 101, 62})

	// This loop writes all bytes until we meet a newline, an octothorpe, and a plus sign.
	// "\n#+" signals that the block is finished and we've hit the "#+END_SRC" line.
	for i := pos; i < len(s)-1; i++ {
		pos++
		// If char is a newline, the char after is an octothorpe, and the char after is a plus
		if s[i] == 10 && s[i+1] == 35 && s[i+2] == 43 {
			break
		} else {
			buf.WriteByte(s[i])
		}
	}

	// This write is the same as "</pre>"
	buf.Write([]byte{60, 47, 112, 114, 101, 62})

	// This block write is the same as "</code>"
	buf.Write([]byte{60, 47, 99, 111, 100, 101, 62})

	// We return the newline after the #+END_SRC line
	return pos + 9
}

// This function will iterate over the given slice of bytes and return
// everything from the char at the given position (POS), until the first
// occurance of either a space or newline. If it hits a space, it will
// continue until it finds the newline, disregarding everything it passes
// over, and the return the character after the newline.
func getLanguage(s []byte, pos int) ([]byte, int) {
	var lang []byte

	for i := pos; i < len(s)-1; i++ {
		// If char is a newline
		if s[i] == 10 {
			return lang, i + 1

			// If char is a space
		} else if s[i] == 32 {
			// Here we loop until we hit the newline then return the slice, and position
			// of the newline
			for s[i] != 10 {
				i++
			}

			return lang, i + 1
		}

		lang = append(lang, s[i])
	}

	// This is here to satisfy the compiler, it won't be reached.
	return lang, len(s)
}
