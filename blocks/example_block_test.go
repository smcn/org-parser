package blocks

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"testing"
)

func TestCreateExampleBlock(t *testing.T) {
	var buf bytes.Buffer

	for i := 1; i < 3; i++ {
		block, err := ioutil.ReadFile(fmt.Sprintf("test_files/example_block_%d.org", i))
		if err != nil {
			t.Fatal(err)
		}

		html, err := ioutil.ReadFile(fmt.Sprintf("test_files/example_block_%d.html", i))
		if err != nil {
			t.Fatal(err)
		}

		_ = createExampleBlock(block, 15, &buf)

		for i := range buf.Bytes() {
			if buf.Bytes()[i] != html[i] {
				t.Logf("\n buf: %v\nhtml: %v", string(buf.Bytes()), string(html))
				t.Fatalf("'%s' doesn't match '%s'", string(buf.Bytes()[i]), string(html[i]))
			}
		}

		buf.Reset()
	}
}
