package blocks

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"testing"
)

func TestCreateSrcBlock(t *testing.T) {
	var buf bytes.Buffer

	for i := 1; i < 3; i++ {
		block, err := ioutil.ReadFile(fmt.Sprintf("test_files/src_block_%d.org", i))
		if err != nil {
			t.Fatal(err)
		}

		html, err := ioutil.ReadFile(fmt.Sprintf("test_files/src_block_%d.html", i))
		if err != nil {
			t.Fatal(err)
		}

		_ = createSrcBlock(block, 12, &buf)

		for i := range buf.Bytes() {
			if buf.Bytes()[i] != html[i] {
				t.Logf("\n buf: %v\nhtml: %v", string(buf.Bytes()), string(html))
				t.Fatalf("'%s' doesn't match '%s'", string(buf.Bytes()[i]), string(html[i]))
			}
		}

		buf.Reset()
	}
}

func TestGetLanguage(t *testing.T) {
	tt := []struct {
		input, output []byte
		endPos        int
	}{
		{[]byte("scheme\n"), []byte("scheme"), 7},
		{[]byte("emacs-lisp\n"), []byte("emacs-lisp"), 11},
		{[]byte("ruby -i :noweb yes\n"), []byte("ruby"), 19},
		{[]byte("go\n"), []byte("go"), 3},
	}

	for _, test := range tt {
		o, p := getLanguage(test.input, 0)

		if len(o) != len(test.output) {
			t.Fatalf("'%s' != '%s'", test.output, o)
		}

		for i, char := range test.output {
			if char != o[i] {
				t.Fatalf("'%s' != '%s'", string(char), string(o[i]))
			}
		}

		if test.endPos != p {
			t.Fatalf("%d != %d", test.endPos, p)
		}
	}
}
