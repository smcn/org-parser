package blocks

import (
	"bytes"
)

// This is the entry point for creating blocks. It essentially just checked the character after
// the underscore then throws everything to the appropriate function. It returns the position of
// the character after the end of the block.
func CreateBlock(s []byte, pos int, buf *bytes.Buffer) int {
	var next int

	switch s[pos] {
	// If char is "S"
	case 83:
		next = createSrcBlock(s, pos+4, buf)

	// If char is "E"
	case 69:
		next = createExampleBlock(s, pos+8, buf)

	// If char is "Q"
	case 81:
		next = createQuoteBlock(s, pos+6, buf)
	}

	return next
}
