package blocks

import (
	"bytes"
)

// This function creates a HTML formatted quote block.
//
// It transforms this:
//  #+BEGIN_QUOTE
//  This is an example quote. ---This comment
//  #+END_QUOTE
//
// Into this:
//  <quote><pre>This is an example quote. ---This comment</pre></quote>
//
// It is expected that you pass the position of the character after the newline to save steps.
// Example, everything within the square brackets gets passed over:
//  [#+BEGIN_QUOTE
//  ]This is an example quote. ---This comment
//
// It will return the position after the end of the block (most likely a newline).
func createQuoteBlock(s []byte, pos int, buf *bytes.Buffer) int {
	// This write is the same as "<quote>"
	buf.Write([]byte{60, 113, 117, 111, 116, 101, 62})

	// This write is the same as "<pre>"
	buf.Write([]byte{60, 112, 114, 101, 62})

	// This loop writes all bytes until we meet a newline, an octothorpe, and a plus sign.
	// "\n#+" signals that the block is finished and we've hit the "#+END_QUOTE" line.
	for i := pos; i < len(s)-1; i++ {
		pos++
		// If char is a newline, the char after is an octothorpe, and the char after is a plus
		if s[i] == 10 && s[i+1] == 35 && s[i+2] == 43 {
			break
		} else {
			buf.WriteByte(s[i])
		}
	}

	// This write is the same as "</pre>"
	buf.Write([]byte{60, 47, 112, 114, 101, 62})

	// This write is the same as "</quote>"
	buf.Write([]byte{60, 47, 113, 117, 111, 116, 101, 62})

	// We return the newline after the #+END_QUOTE line
	return pos + 10
}
