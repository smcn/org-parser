package blocks

import (
	"bytes"
)

// This function creates a HTML formatted example block.
//
// It transforms this:
//  #+BEGIN_EXAMPLE
//  This is an example block.
//  #+END_EXAMPLE
//
// Into this:
//  <example><pre>This is an example quote.</pre></example>
//
// It is expected that you pass the position of the character after the newline to save steps.
// Example, everything within the square brackets gets passed over:
//  [#+BEGIN_EXAMPLE
//  ]This is an example block.
//
// It will return the position after the end of the block (most likely a newline).
func createExampleBlock(s []byte, pos int, buf *bytes.Buffer) int {
	// This write is the same as "<example>"
	buf.Write([]byte{60, 101, 120, 97, 109, 112, 108, 101, 62})

	// This write is the same as "<pre>"
	buf.Write([]byte{60, 112, 114, 101, 62})

	// This loop writes all bytes until we meet a newline, an octothorpe, and a plus sign.
	// "\n#+" signals that the block is finished and we've hit the "#+END_EXAMPLE" line.
	for i := pos; i < len(s)-1; i++ {
		pos++
		// If char is a newline, the char after is an octothorpe, and the char after is a plus
		if s[i] == 10 && s[i+1] == 35 && s[i+2] == 43 {
			break
		} else {
			buf.WriteByte(s[i])
		}
	}

	// This write is the same as "</pre>"
	buf.Write([]byte{60, 47, 112, 114, 101, 62})

	// This write is the same as "</example>"
	buf.Write([]byte{60, 47, 101, 120, 97, 109, 112, 108, 101, 62})

	// We return the newline after the #+END_EXAMPLE line
	return pos + 12
}
