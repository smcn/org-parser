package headings

import (
	"bytes"
	"testing"
)

func TestCreateHeading(t *testing.T) {
	var buf bytes.Buffer

	tt := []struct {
		s       []byte
		expResp string
	}{
		{[]byte("* this is a heading"), "<h1>this is a heading</h1>"},
		{[]byte("** this is a heading"), "<h2>this is a heading</h2>"},
		{[]byte("*** this is a heading"), "<h3>this is a heading</h3>"},
	}

	for _, test := range tt {
		CreateHeading(test.s, 0, &buf)

		if buf.String() != test.expResp {
			t.Fatalf("resp: %s\nexpResp: %s", buf.String(), test.expResp)
		}

		buf.Reset()
	}
}

func TestCountAsterisks(t *testing.T) {
	tt := []struct {
		s       []byte
		expResp byte
	}{
		{[]byte("* "), 49},
		{[]byte("** "), 50},
		{[]byte("*** "), 51},
		{[]byte("********* "), 57},
		{[]byte("******************* "), 57},
	}

	for _, test := range tt {
		resp, _ := countAsterisks(test.s, 0)

		if resp != test.expResp {
			t.Fatalf("resp: %d\nexpResp: %d", resp, test.expResp)
		}
	}
}
