package headings

import (
	"bytes"
	"gitlab.com/smcn/org-parser/textdecoration"
)

// This function creates a HTML formatted heading.
//
// It transforms these:
//  * This is an example heading
//  ** This is an example subheading
//
// Into these:
//  <h1>This is an example heading</h1>
//  <h2>This is an example subheading</h2>
//
// It will return the position of the first occurance of a newline.
func CreateHeading(s []byte, pos int, buf *bytes.Buffer) int {
	depth, pos := countAsterisks(s, pos)

	// This write is the same as <h`depth`>
	buf.Write([]byte{60, 104, depth, 62})

	for i := pos; i < len(s); i++ {
		if s[i] == 10 {
			// Now that we have the end point, we pass it into textdecoration
			textdecoration.Decorate(s[pos:i], buf)

			pos = i
			break
		} else if i == len(s)-1 {
			// Now that we have the end point, we pass it into textdecoration
			textdecoration.Decorate(s[pos:i+1], buf)

			pos = i
			break
		}
	}

	// This write is the same as </h`depth`>
	buf.Write([]byte{60, 47, 104, depth, 62})

	return pos - 1
}

// This function will count the amount of asterisks beginning at s[pos] and return the character
// after the space.
//
// Example:
//  * This is a heading          // returns 1, 2
//  ** This is a subheading      // returns 2, 3
//  *** This is a sub-subheading // returns 3, 4
func countAsterisks(s []byte, pos int) (byte, int) {
	// This is the ASCII code for 0, this saves type casting
	var count byte = 48

	for i := pos; i < len(s)-1; i++ {
		pos++
		if s[i] == 42 {
			// 57 is the ASCII code for 9, 58 is the ASCII code for :
			// Realistically, you would never want a <h10> tag, nor even
			// a <h9>. This check will just ensure that no weird things
			// happen.
			if count < 57 {
				count++
			}
		} else if s[i] == 32 {
			break
		}
	}

	return count, pos
}
