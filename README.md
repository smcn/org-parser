# org-parser

**org-parser** is a Go tool used to transform [Org markup](https://orgmode.org/manual/Markup.html) into HTML. It is still under development and is thus not feature complete. Expect bugs.

## Getting Started

Binaries will be supplied when it reaches 1.0.0. In the mean time, the only dependency required to compile it is Go >= 1.10. Please consult the Go documentation to set up your `$GOPATH`, then `git clone` this into your `$GOPATH`, `cd` into the directory, and run `go build main.go`. That will create a binary for your system and you can, if you wish, place that into your `$PATH`.

The CLI arguments are, right now, fairly barebones. It requires a `-file` and has an optional flag for an output file. If no output file is specified (using `-output`), then the HTML it written to Stdout, which can be redirected where ever you like.

Example:

`org-parser -file blog_post.org -output blog_post.html`

## Current Features

**org-parser** can currently translate:

* Headings
* Tables
* Code Blocks
* Examples Blocks
* Quote Blocks
* Paragraphs
* Text Decoration (bold, italics, and inline code)

## Performance

Performance is a key feature of **org-parser**, especially because I built it with a Raspberry Pi in mind. On my laptop, a Thinkpad T460 with an i5-6200u and 8gb of RAM, it can convert roughly 100 million characters per second.

Here is a terminal benchmark using [perf](https://perf.wiki.kernel.org/index.php/Main_Page):

![perf stat output](images/org-parser-perf.png)
