package main

import (
	"flag"
	"fmt"
	"gitlab.com/smcn/org-parser/parser"
	"io/ioutil"
)

func main() {
	file := flag.String("file", "", "Choose which file to be parsed")
	output := flag.String("output", "", "Choose the name for the created file. If blank, output will be written to Stdout.")

	flag.Parse()

	if *file == "" {
		fmt.Println("Please choose a file to be parsed.")
		return
	}

	f, err := ioutil.ReadFile(*file)
	if err != nil {
		fmt.Println(err)
		return
	}

	html := parser.Parse(f)

	if *output == "" {
		fmt.Println(string(html))
	} else {
		ioutil.WriteFile(*output, html, 0644)
	}
}
