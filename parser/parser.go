package parser

import (
	"bytes"
	"gitlab.com/smcn/org-parser/blocks"
	"gitlab.com/smcn/org-parser/headings"
	"gitlab.com/smcn/org-parser/paragraphs"
	"gitlab.com/smcn/org-parser/tables"
)

// This is the main logic loop. It will iterate through each byte and, send it to the appropriate package.
func Parse(f []byte) []byte {
	var buf bytes.Buffer
	var next int

	for i := 0; i < len(f)-1; i++ {
		// If char is a newline
		if f[i] == 10 {
			switch f[i+1] {
			// If char is another newline
			case 10:
				continue
			// If char is an asterisk ("*")
			case 42:
				// If char is a space or another asterisk
				if f[i+2] == 32 || f[i+2] == 42 {
					next = headings.CreateHeading(f, i+1, &buf)

					i = next
				} else {
					// This is used if the asterisk isn't the start of a heading
					next = paragraphs.CreateParagraph(f, i+1, &buf)

					i = next
				}
			// If char is a pipe ("|")
			case 124:
				next = tables.CreateTable(f, i+1, &buf)

				i = next

			// If char is an octothorpe ("#")
			case 35:

				// Here we check that the next char is a "+"
				if f[i+2] == 43 {

					switch f[i+3] {
					// If char is "B"
					case 66:
						// We're passing i+9 into the function because we're
						// bypassing most of the chars that we will do nothing
						// with. "#+BEGIN_" is what we're skipping.
						next = blocks.CreateBlock(f, i+9, &buf)

						i = next
					}
				}

			default:
				// If the char doesn't match any of the above, then it will be
				// given its own paragraph tags
				next = paragraphs.CreateParagraph(f, i+1, &buf)

				i = next
			}
		} else {
			buf.WriteByte(f[i])
		}
	}

	return buf.Bytes()
}
