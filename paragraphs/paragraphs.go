package paragraphs

import (
	"bytes"
	"gitlab.com/smcn/org-parser/textdecoration"
)

// This function creates a HTML formatted paragraph.
//
// This is the default styling for org markup sections. Nothing fancy,
// just wraps things in <p></p> tags.
func CreateParagraph(s []byte, pos int, buf *bytes.Buffer) int {
	// This write is the same as <p>
	buf.Write([]byte{60, 112, 62})

	// This loops through and establishes the boundaries of the paragraph.
	for i := pos; i < len(s); i++ {
		if s[i] == 10 {
			// Now that we have the end point, we pass it into textdecoration.
			textdecoration.Decorate(s[pos:i], buf)

			pos = i
			break
		} else if i == len(s)-1 {
			// Now that we have the end point, we pass it into textdecoration.
			textdecoration.Decorate(s[pos:i+1], buf)

			pos = i
			break
		}
	}

	// This write is the same as </p>
	buf.Write([]byte{60, 47, 112, 62})

	return pos - 1
}
