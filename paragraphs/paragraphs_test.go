package paragraphs

import (
	"bytes"
	"testing"
)

func TestCreateParagraphs(t *testing.T) {
	var buf bytes.Buffer

	tt := []struct {
		s       []byte
		expResp string
	}{
		{[]byte("This is *an example* paragraph."),
			"<p>This is <b>an example</b> paragraph.</p>"},
		{[]byte("This is /an example/ paragraph."),
			"<p>This is <i>an example</i> paragraph.</p>"},
		{[]byte("This is ~an example~ paragraph."),
			"<p>This is <code>an example</code> paragraph.</p>"},
	}

	for _, test := range tt {
		CreateParagraph(test.s, 0, &buf)

		if test.expResp != buf.String() {
			t.Fatalf("resp: %s\nexpResp: %s", buf.String(), test.expResp)
		}

		buf.Reset()
	}
}
