package tables

import (
	"bytes"
	"gitlab.com/smcn/org-parser/textdecoration"
)

// This takes in an org file (as a slice of bytes) and start position of a table
// and returns a formatted HTML table, as well as the position of the last character.
//
// An org table looks like:
//
//  |-----------+-----------|
//  | heading 1 | heading 2 |
//  |-----------+-----------|
//  | column 1  | column 1  |
//  | column 2  | column 2  |
//  | column 3  | column 3  |
//  |-----------+-----------|
//
// The start of an org table is designated as a pipe ("|") at the beginning of a line.
// The header (the line contained within the |---| rows at the top) is not mandatory.
// Any subsequent dividing line (|---|) will be omitted.
func CreateTable(s []byte, pos int, buf *bytes.Buffer) int {
	var lineLen int = getLineLen(s, pos)
	var end int    // this is the end position of the table
	var row []byte // A row is equal to the current position to the next newline

	// This block of writes is the same as "<table>"
	buf.Write([]byte{60, 116, 97, 98, 108, 101, 62})

	// If char is a pipe ("-"), we can infer that a heading exists.
	if s[pos+1] == 45 {
		// Jump to next line
		pos += lineLen + 1

		row = s[pos : pos+lineLen]

		formatRow(row, []byte{116, 104}, buf)

		// Jump to next line
		pos += lineLen + 1
	}

	for i := pos; i < len(s)-1; i++ {
		// if char is a "|"
		if s[i] == 124 {

			row = s[i : i+lineLen]

			formatRow(row, []byte{116, 100}, buf)

			// Jump to next line
			i += lineLen
		} else {
			end = i
			break
		}
	}

	// This write is the same as "</table>"
	buf.Write([]byte{60, 47, 116, 97, 98, 108, 101, 62})

	// We remove two characters because getLineLen() is giving us newlines,
	// but, we want to be able to operate on the newline after this func.
	return end - 2
}

// This takes a single line of an org table and converts it to HTML.
//
// Tags are a slice of bytes, typically either:
//  []byte{116, 104} for "th"
//  []byte{116, 100} for "td"
func formatRow(r, tag []byte, buf *bytes.Buffer) {
	// If char is a "-"
	if r[1] == 45 {
		return
	}

	// This block of writes is the same as "<tr>"
	buf.Write([]byte{60, 116, 114, 62})

	cells := defineCells(r, nil)

	for i, cell := range cells {
		// This block of writes is the same as "<tag>"
		buf.WriteByte(60)
		buf.Write(tag)
		buf.WriteByte(62)

		textdecoration.Decorate(cell, buf)

		if i+1 < len(cells) {
			// This block of writes is the same as "</tag>"
			buf.Write([]byte{60, 47})
			buf.Write(tag)
			buf.Write([]byte{62})
		}
	}

	// This block of writes is the same as "</tag>"
	buf.Write([]byte{60, 47})
	buf.Write(tag)
	buf.WriteByte(62)

	// This write is the same as "</tr>"
	buf.Write([]byte{60, 47, 116, 114, 62})
}

// This function will convert a row into individual cells, represented as an array of array of bytes.
func defineCells(r []byte, rs [][]byte) [][]byte {
	// If we reach the newline
	if len(r) == 1 {
		return rs
	}

	var cell []byte
	for i := 1; i < len(r); i++ {
		// If the character is a pipe
		if r[i] != 124 {

			if r[i] == 32 &&
				// Here we're checking to see if the current character (CHAR) is a
				// space. If it is, we then check to see if the character after CHAR
				// is a space, or if it's a pipe, we then check to see if the
				// character before CHAR is a pipe. If any of these conditions are
				// met, we skip CHAR.
				//
				// Because org tables format themselves, there will only ever be one
				// space at the start of a cell, that's why, if there is a pipe
				// before CHAR, there will not be another space after CHAR.
				((r[i+1] == 32 || r[i+1] == 124) || r[i-1] == 124) {

				continue
			} else {
				cell = append(cell, r[i])
			}
		} else {
			rs = append(rs, cell)
			rs = defineCells(r[i+1:], rs)

			break
		}
	}

	return rs
}

// This returns to amount of characters until the occurance of a newline.
func getLineLen(s []byte, pos int) int {
	var count int

	for i := pos; i < len(s)-1; i++ {
		// if char is a newline
		if s[i] != 10 {
			count++
		} else {
			break
		}
	}

	return count
}
