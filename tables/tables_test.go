package tables

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"testing"
)

func TestCreateTable(t *testing.T) {
	var buf bytes.Buffer

	for i := 1; i < 4; i++ {
		table, err := ioutil.ReadFile(fmt.Sprintf("test_files/table_%d.org", i))
		if err != nil {
			t.Fatal(err)
		}

		html, err := ioutil.ReadFile(fmt.Sprintf("test_files/table_%d.html", i))
		if err != nil {
			t.Fatal(err)
		}

		_ = CreateTable(table, 0, &buf)

		for i := range buf.Bytes() {
			if buf.Bytes()[i] != html[i] {
				t.Logf("\n buf: %v\nhtml: %v", string(buf.Bytes()), string(html))
				t.Fatalf("'%s' doesn't match '%s'", string(buf.Bytes()[i]), string(html[i]))
			}
		}

		buf.Reset()
	}
}

func TestFormatRow(t *testing.T) {
	var buf bytes.Buffer

	tt := []struct {
		row, tag, expectedResult string
	}{
		{"| test | row |", "td", "<tr><td>test</td><td>row</td></tr>"},
		{"|---+----|", "th", ""},
		{"| test | row | a gain |", "crazy", "<tr><crazy>test</crazy><crazy>row</crazy><crazy>a gain</crazy></tr>"},
	}

	for _, r := range tt {
		formatRow([]byte(r.row), []byte(r.tag), &buf)

		if buf.String() != r.expectedResult {
			t.Fatalf("buf.String() didn't match the expected result\nBuf: %s\nResult: %s", buf.String(), r.expectedResult)
		}

		buf.Reset()
	}
}

func TestDefineCells(t *testing.T) {
	tt := []struct {
		s       []byte
		expResp [][]byte
	}{
		{[]byte(" this is | is | a | test |"),
			[][]byte{[]byte("this is"), []byte("is"), []byte("a"), []byte("test")}},
		{[]byte(" this    is | is | a | test |"),
			[][]byte{[]byte("this is"), []byte("is"), []byte("a"), []byte("test")}},
	}

	for _, test := range tt {
		resp := defineCells(test.s, nil)

		if len(resp) != len(test.expResp) {
			t.Fatalf("Length is not the same:\nresp: %+v\nexpResp: %+v", resp, test.expResp)
		}

		for i, _ := range test.expResp {
			for j, char := range test.expResp[i] {
				if resp[i][j] != char {
					t.Fatalf("resp: %+v\nexpResp: %+v", resp, test.expResp)
				}
			}
		}
	}
}

func TestGetLineLen(t *testing.T) {
	tt := []struct {
		str                      string
		startingPos, expectedlen int
	}{
		{"this is a test string", 0, 20},
		{"this is another test string", 0, 26},
		{"this is yet another test string", 0, 30},
		{`this is a
multiline string`, 10, 15},
		{`this is a
multiline string but it contains
a third line`, 10, 32},
	}

	for _, l := range tt {
		result := getLineLen([]byte(l.str), l.startingPos)
		if result != l.expectedlen {
			t.Fatalf("Length of line ('%d') doesn't match getLineLen ('%d')",
				l.expectedlen, result)
		}
	}
}
